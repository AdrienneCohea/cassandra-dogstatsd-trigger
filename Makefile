.PHONY: clean all

all:
	gradle shadow
	mkdir -p ./out
	cp ./lib/build/libs/lib-all.jar ./out
	docker-compose up --detach --force-recreate

clean:
	docker-compose down
	sudo rm -rf ./out
	gradle clean
